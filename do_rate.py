import json

# * Подготовка данных
# - выделить код биржи в класс
# - сохранить котировки в формате выбранного хранилища
# - создать алгоритм автоматической конвертации любой валюты к BTC
# - привести базовую или котируемою валюту к BTC для определения волатильности пары
# - фильтрация валютных пар раз в час по уровню волатильности
# - поиск кольца
# - мониторинг найденного кольца
#
import Exchange

th = {}


def init():
    # прочитаем параметры запуска
    file = open('goldenring.json')
    json_data = json.load(file)
    file.close()
    exchanges = json_data['exchanges']
    # Создадим запись для каждой биржи
    for exch in exchanges:
        th[exch['exchange']] = None  # Exchange.run_exchange(exch)


def run_ring(id_exchange):
    # прочитаем параметры запуска
    file = open('goldenring.json')
    json_data = json.load(file)
    file.close()
    exchange = [item for item in json_data['exchanges'] if item['exchange'] == id_exchange][0]
    th[id_exchange] = Exchange.run_exchange(exchange)
    th[id_exchange]['profile'].start()
    th[id_exchange]['findrings'].start()



def get_exchanges():
    file = open('goldenring.json')
    json_data = json.load(file)
    file.close()
    # exchanges = [item['exchange'] for item in json_data['exchanges']]
    exchanges = json_data['exchanges']
    return exchanges


def get_status():
    status = {}
    for t in th:
        obj = th[t]
        if obj is None:
            status[t] = {'profile': False, 'findrings': False}
        else:
            status[t] = {'profile': obj['profile'].is_alive(), 'findrings': obj['findrings'].is_alive()}
    return status


def exchange_stop(id_excange=None):
    try:
        obj = th[id_excange]
        th[id_excange] = None

        t1=obj['findrings']
        t2=obj['profile']

        t1.stopevent = True


        t2.stopevent = True
        # t1.join()
        # t2.join()
    except:
        return {'error': True, 'msg': f"Ошибка при остановке '{id_excange}'"}
    return get_status()


def exchange_start(id_exchange=None):
    try:
        status = get_status()[id_exchange]
        if status['profile'] == False and status['findrings'] == False:
            run_ring(id_exchange)
    except:
        return {'error': True, 'msg': f"Ошибка при запуске '{id_exchange}'"}
    return get_status()

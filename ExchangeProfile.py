from threading import Thread, Event
import ccxt
import sqlite3
import time
import logging


class ExchangeProfile(Thread):
    def __init__(self, param):
        logging.basicConfig(filename="exchangeprofile.log", filemode="w", level=logging.INFO)
        self.log = logging.getLogger("ExchangeProfile")
        Thread.__init__(self, name=param['exchange'] + "_profile", daemon=True)
        #TODO получать параметры каждый раз из конфига, чтоб вносить изменения без перезагрузки
        self.param = param
        self.exchange = self.get_exchange(param['exchange'])
        self.stopevent = False
        # t = self.exchange.fetch_tickers()
        # Члены класса
        self.log.info("__init__ done")
        pass

    def run(self):
        self.log.info("run() start")
        print('Поток ExchangeProfile запущен')
        # Проверяем что есть таблица в БД. Если нет - создаем
        self.check_table()

        # Основной цикл
        try:
            while not self.stopevent:
                self.update_exchange_profile()
                time.sleep(30)
        except Exception as e:
            self.log.info(f"Ошибка в основном потоке: {e}")

        # Конец основного цикла
        print('Поток ExchangeProfile остановлен')
        self.log.info("run() done")

    def get_exchange(self, exchange_id):
        """ Создать класс для работы с биржей"""
        # подключимся к бирже по имени
        exchange_class = getattr(ccxt, exchange_id)
        return exchange_class({})

    def update_exchange_profile(self):
        tickers=self.fetch_tickers()
        tickers = self.calc_volume_in_btc(tickers)
        self.update_symbols_in_db(tickers)
        filtered_symbols = self.filter_symbols(tickers)
        self.save_symbols_to_db(filtered_symbols)
        pass

    def check_table(self):
        """ Создать в БД таблицу если ее нет """
        self.log.info("check_table() start")
        exchange = self.param['exchange']
        sql = f"""
              create table if not exists {exchange}
              (
              symbol text not null
              primary key,
              btcvolume real default 0.0 not null,
              active int default 0 not null
              )
              """
        connect = self.open_db()
        cursor = connect.cursor()
        rz = cursor.execute(sql)
        connect.close()
        self.log.info("check_table() done")
        pass

    def open_db(self):
        """ Создаем подключение к БД """
        conn: sqlite3.Connection = sqlite3.connect("goldenring.sqlite3", check_same_thread = False)
        return conn

    def fetch_symbols(self):
        """получим валютные пары с биржи"""
        self.exchange.load_markets()
        return self.exchange.symbols

    def update_symbols_in_db(self, tickers):
        """ Синхронизируем символы с БД - добавляем"""
        connect = self.open_db()
        cursor = connect.cursor()
        for symbol in tickers:
            sql = f"""
            insert into {self.param['exchange']}(symbol, btcvolume) 
            values('{symbol}', '{tickers[symbol]['btcVolume']}')
            on conflict(symbol) do update set btcvolume = '{tickers[symbol]['btcVolume']}'  
            """
            cursor.execute(sql)

        connect.commit()
        connect.close()
        pass

    def filter_symbols(self, tickers):
        # TODO добавить фильтр на regexp
        # exclude_symbol = self.param['exclude_symbols']
        # symbols = sorted(list(set(exchange_symbol) - set(exclude_symbol)))
        min_btc_volume = self.param['min_btc_volume']
        ftickers=[]
        for s in tickers:
            if tickers[s]['btcVolume'] < min_btc_volume:
                continue
            ftickers.append(s)

        return ftickers


    def save_symbols_to_db(self, symbols):
        """ Синхронизируем символы с БД - включаем отфильтрованные"""
        # TODO: добавить проверку на ошибки
        connect = self.open_db()
        cursor = connect.cursor()
        # отключим все валюты
        sql = f"update {self.param['exchange']} set active=0"
        cursor.execute(sql)

        # включим отфильтрованные пары
        for symbol in symbols:
            sql = f"update {self.param['exchange']} set active = 1 where symbol = '{symbol}'"
            cursor.execute(sql)

        connect.commit()
        connect.close()
        pass

    def fetch_tickers(self):
        """получить данные по тикерам"""
        # TODO переписать на групповую операцию get_tickers() с ограничением длины 250 символов
        #
        tickers = {}
        fetch_tickers_mode = self.param['fetch_tickers_mode']

        # if fetch_tickers_mode == 1:
        #     for symbol in symbols:
        #         try:
        #             ticker = self.exchange.fetch_ticker(symbol)
        #         except BaseException:
        #             print("ticker " + symbol + " has error.")
        #             # tickers[symbol] = None
        #             continue
        #         # print(symbol)
        #         base, quote = symbol.split("/")
        #         tickers[symbol] = {'base': base, 'quote': quote, 'ask': ticker['ask'], 'bid': ticker['bid'],
        #                            'baseVolume': ticker['baseVolume'],
        #                            'quoteVolume': ticker['quoteVolume']}
        #
        # if fetch_tickers_mode == 2:
        #     ml = self.exchange.options.get('fetchTickersMaxLength')
        #     # symbols2=symbols.copy()
        #     tickers_array=[]
        #     ticker_len=0
        #
        #     for symbol in symbols:
        #         tickers_array.append()
        #         pass
        #
        #     tickers_raw = self.exchange.fetch_tickers()


        if fetch_tickers_mode == 3:
            tickers_raw = self.exchange.fetch_tickers()
            for ticker in tickers_raw:
                base, quote = ticker.split("/")
                # проверяем что все объемы вычисленны
                if tickers_raw[ticker]['quoteVolume'] == None:
                    tickers_raw[ticker]['quoteVolume'] = tickers_raw[ticker]['baseVolume'] * tickers_raw[ticker]['bid']
                if tickers_raw[ticker]['baseVolume'] == None:
                    tickers_raw[ticker]['baseVolume'] = tickers_raw[ticker]['quoteVolume'] * tickers_raw[ticker]['ask']

                tickers[ticker] = {'base': base, 'quote': quote, 'ask': tickers_raw[ticker]['ask'], 'bid': tickers_raw[ticker]['bid'],
                                   'baseVolume': tickers_raw[ticker]['baseVolume'],
                                   'quoteVolume': tickers_raw[ticker]['quoteVolume']}


        return tickers
        pass

    def calc_volume_in_btc(self, tickers):
        """вычислить объем сделок за час в BTC"""
        tickers2 = tickers.copy()
        for t in tickers2:
            ticker = tickers2[t]

            # if (ticker['bid'] == 0) or (ticker['ask'] == 0):
            #     ticker['btcVolume'] = int(0)
            #     continue

            # если есть BTC - присваиваем это значение в btcvolume
            if ticker['base'] == "BTC":
                ticker['btcVolume'] = int(ticker['baseVolume'])
                continue
            if ticker['quote'] == "BTC":
                ticker['btcVolume'] = int(ticker['quoteVolume'])
                continue
            # если нет - ищем в списке котировку и пересчитываем по ней
            # 1 - ищем по базовой валюте
            s = ticker['base'] + "/BTC"
            qticker = tickers2.get(s)
            if qticker != None:
                ticker['btcVolume'] = int(ticker['baseVolume'] * qticker['bid'])
                continue
            # 2 - ищем по котируемой валюте
            s = ticker['quote'] + "/BTC"
            qticker = tickers2.get(s)
            if qticker != None:
                ticker['btcVolume'] = int(ticker['quoteVolume'] * qticker['bid'])
                continue
            # если ничего не нашли - 'btcvolume'=0 для удаления
            ticker['btcVolume'] = int(0)
        return tickers2


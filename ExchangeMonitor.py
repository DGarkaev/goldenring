import collections
import ccxt
from threading import Thread
from threading import Lock
import json
import time
from flask import jsonify


def monitor(ring, param):
    """принимаем цепочку и запускаем тред для ее мониторинга"""

    # проверим что цепочка еще не существует
    id_ring = ring['id']
    if id_ring in ExchangeMonitor.rings_list:
        if ExchangeMonitor.rings_list[id_ring] == 1:
            return

    # print(f"add {id_ring}")
    ExchangeMonitor.rings_list[id_ring] = 1
    exchange_monitor = ExchangeMonitor(ring, param)
    exchange_monitor.start()


class ExchangeMonitor(Thread):
    lock = Lock()
    # экземпляр класса, здесь хранятся все цепочки для отслеживания дублирования
    rings_list = collections.defaultdict(dict)
    ring = {}

    def __init__(self, ring, param):
        Thread.__init__(self, name=f"monitor_{ring['id']}", daemon=True)
        self.ring = ring
        self.param = param

        # подключимся к бирже
        exchange_id = self.ring['exchange']
        exchange_class = getattr(ccxt, exchange_id)
        self.exchange = exchange_class({})
        self.exchange.load_markets()
        self.symbols = self.exchange.symbols

    def run(self):
        # TODO вставить расчет комиссии
        fee = self.param['fee_taker']
        while True:
            order_books = []

            for symbol in self.ring['pairs']:
                ob = self.get_order_book(symbol)
                order_books.append(ob)

            dt: float
            z: float

            if len(order_books) == 4:
                a1 = order_books[0]['amount']
                p1 = order_books[0]['price']
                s1 = order_books[0]['symbol'].split('/')
                a0 = a1
                z = a0 * p1
                a2 = order_books[1]['amount']
                p2 = order_books[1]['price']
                s2 = order_books[1]['symbol'].split('/')
                if z > a2:
                    a0 = a2 / p1
                z = a0 * p1 * p2
                a3 = order_books[2]['amount']
                p3 = order_books[2]['price']
                s3 = order_books[2]['symbol'].split('/')
                if z > a3:
                    a0 = a3 / (p1 * p2)
                z = a0 * p1 * p2 * p3
                a4 = order_books[3]['amount']
                p4 = order_books[3]['price']
                s4 = order_books[3]['symbol'].split('/')
                if z > a4:
                    a0 = a4 / (p1 * p2 * p3)
                z = a0 * p1 * p2 * p3 * p4
                dt = z - a0
                # формируем запрос Барыге
                b={}
                b['exchange'] = self.param['exchange']
                b['pairs']=[
                    {'symbol':order_books[0]['symbol'],
                     'type':order_books[0]['operation'],
                     'amount':a0},

                    {'symbol':order_books[1]['symbol'],
                     'type':order_books[1]['operation'],
                     'amount':a0*p1},

                    {'symbol': order_books[2]['symbol'],
                     'type': order_books[2]['operation'],
                     'amount': a0*p1*p2},

                    {'symbol': order_books[3]['symbol'],
                     'type': order_books[3]['operation'],
                     'amount': a0*p1*p2*p3}
                ]
                job = self.param['rdmq'].enqueue(json.dumps(b))
                pass

            if len(order_books) == 3:
                a1 = order_books[0]['amount']
                p1 = order_books[0]['price']
                s1 = order_books[0]['symbol'].split('/')
                a0 = a1
                z = a0 * p1
                a2 = order_books[1]['amount']
                p2 = order_books[1]['price']
                s2 = order_books[1]['symbol'].split('/')
                if z > a2:
                    a0 = a2 / p1
                z = a0 * p1 * p2
                a3 = order_books[2]['amount']
                p3 = order_books[2]['price']
                s3 = order_books[2]['symbol'].split('/')
                if z > a3:
                    a0 = a3 / (p1 * p2)
                z = a0 * p1 * p2 * p3
                dt = z - a0
                # формируем запрос Барыге
                b = {}
                b['exchange'] = self.param['exchange']
                b['pairs'] = [
                    {'symbol': order_books[0]['symbol'],
                     'type': order_books[0]['operation'],
                     'amount': a0},

                    {'symbol': order_books[1]['symbol'],
                     'type': order_books[1]['operation'],
                     'amount': a0 * p1},

                    {'symbol': order_books[2]['symbol'],
                     'type': order_books[2]['operation'],
                     'amount': a0 * p1 * p2},
                ]
                job = self.param['rdmq'].enqueue(json.dumps(b))
                pass

            ExchangeMonitor.rings_list[self.ring['id']] = 0
            # кольцо сдохло
            # if dt < 0.0:
            #     ExchangeMonitor.rings_list[self.ring['id']]=0
            # ExchangeMonitor.rings_list.pop(self.ring['id'],None)
            # return

            print(self.ring['id'])
            print(f"{s1[0]}: {z:.8f} - {a0:.8f} = {dt:.8f}")

            # time.sleep(1)
            return

    ################################################################################################

    def get_order_book(self, symbol):
        sym = symbol
        dtype: str
        if sym in self.symbols:
            order_book = self.exchange.fetch_l2_order_book(sym, limit=10)
            ob = order_book['bids'][:1]
            price = ob[0][0]
            amount = ob[0][1]
            dtype = 'd'
        else:
            sym = symbol.split('/')
            sym = sym[1] + '/' + sym[0]
            order_book = self.exchange.fetch_l2_order_book(sym, limit=10)
            ob = order_book['asks'][:1]
            price = 1 / ob[0][0]
            # price = 1/price
            # amount = ob[0][1]
            amount = ob[0][1] * ob[0][0]
            dtype = 'r'
            # price[0][1] = price[0][1]/price[0][0]
            # price[0][0] = 1 / price[0][0]

        ob = {}
        ob['symbol'] = sym
        ob['type'] = dtype
        ob['operation']='sell' if dtype == 'r' else 'buy'
        ob['price'] = price
        ob['amount'] = amount

        return ob
        pass

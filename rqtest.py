#!/usr/bin/pyhon3

from rq import Queue
import redis
import os.path
import time
import json

if os.path.exists('dev'):
    rd = redis.Redis(host='178.157.82.121', port=6379)
else:
    rd = redis.Redis(host='redisHuckster')
queue = Queue(name='tasks', connection=rd)
t = time.time()
b = {
    'id': t,
    'msg': f"test of {t}"
}
job = queue.enqueue(json.dumps(b))
time.sleep(5)
print(job.result)

from flask import Flask, render_template, jsonify
from gevent.pywsgi import WSGIServer

import do_rate

# configuration
DATABASE = '/tmp/flaskr.db'
# DEBUG = True
# SECRET_KEY = 'development key'
# USERNAME = 'admin'
# PASSWORD = 'default'

app = Flask(__name__)
app.config.from_object(__name__)


# инициализация системы
do_rate.init()

@app.route('/')
def index():
    return ''

@app.route('/doc')
def doc():
    return render_template('doc.html')

@app.route('/exchanges')
def getexchange():
    rz = do_rate.get_exchanges()
    return jsonify(rz)

@app.route('/status')
def getstatus():
    rz = do_rate.get_status()
    return jsonify(rz)

@app.route('/start/<exchange>')
def exstart(exchange):
    rz = do_rate.exchange_start(exchange)
    return jsonify(rz)

@app.route('/stop/<exchange>')
def exstop(exchange):
    rz = do_rate.exchange_stop(exchange)
    return jsonify(rz)

if __name__ == '__main__':
    # app.run()
    http_server = WSGIServer(('', 8000), app)
    http_server.serve_forever()

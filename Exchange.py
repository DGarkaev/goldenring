import ccxt
from threading import Thread
import sqlite3
import ExchangeProfile as eprof
import time
import collections
import json
import ExchangeMonitor as monitor
from rq import Queue
import redis
import os.path

#################################################################
def run_exchange(param):
    # TODO подумать над архитектурой. Может обьеденить 2 класса в один
    exchange_profile = eprof.ExchangeProfile(param)
    # exchange_profile.start()
    exchange = Exchange(param)
    # exchange.start()
    return {'profile':exchange_profile, 'findrings':exchange}


##################################################################
class Exchange(Thread):
    def __init__(self, param):
        Thread.__init__(self, name=param['exchange']+'findrings', daemon=True)
        if os.path.exists('dev'):
            self.rd=redis.Redis(host='178.157.82.121', port=6379)
        else:
            self.rd = redis.Redis(host='redisHuckster')
        self.queue = Queue(name='tasks', connection=self.rd)
        # tasks, host редиса redisHuckster
        # queue = Queue(connection=)

        self.param = param
        self.param['rdmq']=self.queue
        # подключимся к бирже по имени
        exchange_id = param['exchange']
        exchange_class = getattr(ccxt, exchange_id)
        self.exchange = exchange_class({})
        self.MAIN_ACTIVE = self.param['main_active']
        self.profit_percent = self.param['profit_percent']
        self.fee_maker = self.param['fee_maker']
        self.fee_taker = self.param['fee_taker']

        # загрузим информацию о бирже
        self.exchange.load_markets()

        # подключимся к БД
        self.connect = sqlite3.connect("goldenring.sqlite3", check_same_thread=False)
        self.cursor = self.connect.cursor()

        self.stopevent = False

        pass

    def run(self):
        """Запуск потока"""
        print('Поток FindRing запущен')
        # основной цикл
        while not self.stopevent:
            # try:
            tickers = self.get_tickers()
            dict = self.create_dictionary(tickers)
            # print("=" * 80)
            # print (self.exchange.id)
            # print("=" * 80)
            # self.find_chains1(dict)

            self.find_chains2(dict)
            self.find_chains3(dict)

            time.sleep(15)
            # расчеты
            pass
        # except Exception as e:
        #     print(e)
        #     continue
        # конец основного цикла

        print('Поток FindRing остановлен')

    def fetch_symbols(self):
        sql = f"select symbol from {self.param['exchange']} where active=1"
        self.cursor.execute(sql)
        symbols = [item[0] for item in self.cursor.fetchall()]
        return symbols

    def symbols_split(self, lst, n):
        return [lst[i:i + n] for i in range(0, len(lst), n)]

    def get_tickers(self):
        # TODO сделать нормальный расчет по длине символов
        symbols = self.fetch_symbols()
        # TODO если база пуста, то symbols = None тогда просто выходим
        split = self.symbols_split(symbols, 25)
        tickers = {}
        # если биржа не возвращает часть тикеров, то нет смысла запрашивать по частям,
        # в этом случае получаем все тикеры, и отфильтровываем нужные
        for s in split:
            t = self.exchange.fetch_tickers(s)
            # если вернулось больше чем мы запрашивали - значит биржа не может отдавать
            #  тикеры частями, а отдает все сразу
            if len(t) > len(s):
                # выбираем только необходимые символы
                for s in symbols:
                    tickers[s] = t[s]
                break
            tickers.update(t)

        return tickers

    def create_dictionary(self, tickers):
        dictionary = collections.defaultdict(dict)

        for pair in tickers:
            p: str = pair.upper()

            # есть странный баг - иногда bid и ask = 0,
            # пропустим такие тикеры
            if tickers[p]['bid'] == 0 or tickers[p]['ask'] == 0:
                continue

            p: str = pair.upper()
            src, dst = p.split('/')

            src_ask = {
                'pair': src + '/' + dst,
                'price': float(tickers[p]['bid']),
                'amount': None  # float(ask_order[1])
            }

            src_bid = {
                'pair': dst + '/' + src,
                # TODO Проверить что делим на bid, а не на ask
                'price': 1 / float(tickers[p]['ask']),
                'amount': None  # float(bid_order[1]) * float(bid_order[0])
            }

            dictionary[src][dst] = src_ask
            dictionary[dst][src] = src_bid

        return dict(dictionary)

    def get_route(self, symbol_start, tickers: dict):
        t = tickers.get(symbol_start)
        return t

    def find_chains1(self, tickers):
        """Поиск 1-х цепочек"""
        fee = self.fee_taker
        # цикл по валютам, для которых считаются кольца
        for active in self.MAIN_ACTIVE:
            t0 = self.get_route(active, tickers)
            if t0 is None:
                continue

            for a1 in t0:
                k1 = tickers[active][a1]['price']
                k1 = k1 - k1 * fee / 100
                t1 = self.get_route(a1, tickers)
                if t1 is None:
                    continue
                # a2 = tickers.get(a1)
                a2 = t1.get(active)
                if a2 is None:
                    continue
                k2 = a2['price']
                k2 = k2 - k2 * fee / 100
                # закрыть кольцо
                k = k1 * k2
                kp = (k - 1) * 100
                if kp >= self.profit_percent:
                    print(
                        f"{tickers[active][a1]['pair']}={k1:.0f} => "
                        f"{tickers[a1][active]['pair']} = {k:.4f} => {kp:.2f}%"
                    )
                pass
        print("=" * 80)
        pass

    def find_chains2(self, tickers):
        """Поиск 2-х цепочек"""
        fee = self.fee_taker
        # цикл по валютам, для которых считаются кольца
        for active in self.MAIN_ACTIVE:
            t0 = self.get_route(active, tickers)
            if t0 is None:
                continue

            for a1 in t0:
                k1 = tickers[active][a1]['price']
                k1 = k1 - k1 * fee / 100
                t1 = self.get_route(a1, tickers)
                if t1 is None:
                    continue

                for a2 in t1:
                    k2 = tickers[a1][a2]['price']
                    k2 = k2 - k2 * fee / 100
                    t2 = self.get_route(a2, tickers)
                    if t2 is None:
                        continue
                    a3 = tickers.get(a2)
                    a3 = a3.get(active)
                    if a3 is None:
                        continue
                    k3 = tickers[a2][active]['price']
                    k3 = k3 - k3 * fee / 100
                    # закрыть кольцо
                    k = k1 * k2 * k3
                    kp = (k - 1) * 100
                    if kp >= self.profit_percent:
                        print(
                            f"[{self.exchange.id}]:[2x]:{tickers[active][a1]['pair']}={k1:.4f} "
                            f"=> {tickers[a1][a2]['pair']}={k1*k2:.4f} => "
                            f"{tickers[a2][active]['pair']} = {k:.4f} => {kp:.2f}%"
                        )
                        id = f"{self.exchange.id}:{tickers[active][a1]['pair']}:" \
                             f"{tickers[a1][a2]['pair']}:{tickers[a2][active]['pair']}"
                        j = {}
                        j['id'] = id
                        j['exchange'] = self.exchange.id
                        j['pairs'] = [tickers[active][a1]['pair'],
                                      tickers[a1][a2]['pair'],
                                      tickers[a2][active]['pair']]
                        monitor.monitor(j, self.param)

                    pass
        # print("=" * 80)
        pass

    def find_chains3(self, tickers):
        """Поиск 3-х цепочек"""
        fee = self.fee_taker
        # цикл по валютам, для которых считаются кольца
        for active in self.MAIN_ACTIVE:
            t0 = self.get_route(active, tickers)
            if t0 is None: continue

            for a1 in t0:
                k1 = tickers[active][a1]['price']
                k1 = k1 - k1 * fee / 100
                t1 = self.get_route(a1, tickers)
                if t1 is None: continue

                for a2 in t1:
                    k2 = tickers[a1][a2]['price']
                    k2 = k2 - k2 * fee / 100
                    t2 = self.get_route(a2, tickers)
                    if t2 is None: continue

                    for a3 in t2:
                        k3 = tickers[a2][a3]['price']
                        k3 = k3 - k3 * fee / 100
                        t3 = self.get_route(a3, tickers)
                        if t3 is None: continue
                        a4 = tickers.get(a3)
                        a4 = a4.get(active)
                        if a4 is None: continue
                        k4 = tickers[a3][active]['price']
                        k4 = k4 - k4 * fee / 100
                        # закрыть кольцо
                        k = k1 * k2 * k3 * k4
                        kp = (k - 1) * 100
                        if kp >= self.profit_percent:
                            print(
                                f"[{self.exchange.id}]:[3x]:{tickers[active][a1]['pair']}="
                                f"{k1:.4f} => {tickers[a1][a2]['pair']}={k1*k2:.4f} => "
                                f"{tickers[a2][a3]['pair']}={k1*k2*k3:.4f} => "
                                f"{tickers[a3][active]['pair']} = {k:.4f} => {kp:.2f}%"
                            )
                            id = f"{self.exchange.id}:{tickers[active][a1]['pair']}:" \
                                 f"{tickers[a1][a2]['pair']}:{tickers[a2][a3]['pair']}:" \
                                 f"{tickers[a3][active]['pair']}"
                            j = {}
                            j['id'] = id
                            j['exchange'] = self.exchange.id
                            j['pairs'] = [tickers[active][a1]['pair'], tickers[a1][a2]['pair'],
                                          tickers[a2][a3]['pair'],
                                          tickers[a3][active]['pair']]
                            monitor.monitor(j, self.param)
                            pass

        # print("=" * 80)
        pass
# binance+
#   fee:0.1

# exmo+
# не возвращает часть тикеров
#   "fee_taker" : 0.2,
#   "fee_maker": 0.2,

# yobit

# kraken+
#   "fee_taker" : 0.26,
#   "fee_maker": 0.16,

# kucoin
#   fee:0.1

# cex
#   "fee_taker" : 0.25,
#   "fee_maker": 0.16,
